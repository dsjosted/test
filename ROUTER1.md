conf t
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
hostname Routertest
boot-start-marker
no aaa new-model
memory-size iomem 15
no ip icmp rate-limit unreachable
ip cef
no ip domain lookup
ip auth-proxy max-nodata-conns 3
ip admission max-nodata-conns 3
username jdoe privilege 15 password 0 cisco
ip tcp synwait-time 5
interface FastEthernet0/0
ip address 192.168.1.10 255.255.255.0
duplex auto
speed auto
no shut
exit
interface FastEthernet0/1
ip address 192.168.0.11 255.255.255.0
duplex auto
speed auto
no shut
exit
ip forward-protocol nd
ip route 0.0.0.0 0.0.0.0 10.11.202.140
no ip http server
no ip http secure-server
control-plane
line con 0
exec-timeout 0 0
privilege level 15
logging synchronous
login local
exit
line aux 0
exec-timeout 0 0
privilege level 15
logging synchronous
exit
line vty 0 4
login local
exit